package nl.utwente.di.bookQuote;

import java.util.HashMap;

public class Quoter {
    static HashMap<String, Double> prices;
    static {
        prices = new HashMap<>();
        prices.put("1", 10.0);
        prices.put("2", 45.0);
        prices.put("3", 20.0);
        prices.put("4", 35.0);
        prices.put("5", 50.0);
    }
    public double getBookPrice(String isbn){
        return prices.getOrDefault(isbn, 0.0);
    }
}
